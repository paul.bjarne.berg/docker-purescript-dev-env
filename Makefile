.PHONY: image publish

IMAGE_NAME := purescript-dev-env

image:
	docker build -t="bergp/$(IMAGE_NAME)" -f="./Dockerfile" .

enter:
	docker run -it --rm bergp/$(IMAGE_NAME) bash

publish:
	docker push bergp/$(IMAGE_NAME)
